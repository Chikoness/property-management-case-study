package com.example.propertymanagementapi.exceptionhandlers;

public class NotFoundException extends RuntimeException{
    public NotFoundException(Long id) {
        super("Could not find " + id);
    }
}
