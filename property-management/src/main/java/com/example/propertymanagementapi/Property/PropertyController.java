package com.example.propertymanagementapi.Property;

import com.example.propertymanagementapi.exceptionhandlers.NotFoundException;
import com.sipios.springsearch.anotation.SearchSpec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class PropertyController {
    private final PropertyRepository propertyRepository;
    private final PropertyModelAssembler assembler;

    PropertyController(PropertyRepository propertyRepository, PropertyModelAssembler assembler) {
        this.propertyRepository = propertyRepository;
        this.assembler = assembler;
    }

    // View all properties
    @GetMapping("/api/properties/")
    CollectionModel<EntityModel<Property>> all() {
        List<EntityModel<Property>> properties = propertyRepository.findAll().stream()
                .map(assembler::toModel)
                .collect(Collectors.toList());

        return CollectionModel.of(properties, linkTo(methodOn(PropertyController.class)
                .all()).withSelfRel());
    }

    // View an existing property
    @GetMapping("/api/properties/{id}")
    EntityModel<Property> one(@PathVariable Long id) {

        Property property = propertyRepository.findById(id) //
                .orElseThrow(() -> new NotFoundException(id));

        return assembler.toModel(property);
    }

    // Search for a property
    // Append the url to search. For Example, /api/p?search=state:'Selangor'
    @GetMapping("/api/p")
    ResponseEntity<List<Property>> search(@SearchSpec Specification<Property> specs) {
        return new ResponseEntity<>(propertyRepository.findAll(Specification.where(specs)), HttpStatus.OK);
    }

    // Create a new property
    @PostMapping("/api/properties")
    Property newProperty(@RequestBody Property newProperty) {
        return propertyRepository.save(newProperty);
    }

    // Update an existing property
    @PutMapping("/api/properties/{id}")
    Property replaceProperty(@RequestBody Property newProperty, @PathVariable Long id) {
        return propertyRepository.findById(id)
                .map(property -> {
                    property.setName(newProperty.getName());
                    property.setAddress(newProperty.getAddress());
                    property.setPostcode(newProperty.getPostcode());
                    property.setCity(newProperty.getCity());
                    property.setState(newProperty.getState());
                    return propertyRepository.save(property);
                })
                .orElseGet(() -> {
                    newProperty.setId(id);
                    return propertyRepository.save(newProperty);
                });
    }

    // Approve a property
    @PutMapping("/api/properties/{id}/approval")
    Property setPropertyApproval(@RequestBody Property newProperty, @PathVariable Long id) {
        return propertyRepository.findById(id).map(property -> {
                    property.setPropertyApproved(newProperty.isPropertyApproved());
                    return propertyRepository.save(property);
                }).orElseThrow(() -> new NotFoundException(id));
    }

    // Delete an existing property
    @DeleteMapping("/api/properties/{id}")
    ResponseEntity<?> deleteProperty(@PathVariable Long id) {
        propertyRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}