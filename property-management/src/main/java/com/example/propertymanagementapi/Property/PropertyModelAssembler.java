package com.example.propertymanagementapi.Property;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class PropertyModelAssembler implements RepresentationModelAssembler<Property, EntityModel<Property>> {
    @Override
    public EntityModel<Property> toModel(Property property) {
        return EntityModel.of(property,
                            linkTo(methodOn(PropertyController.class).one(property.getId()))
                                  .withSelfRel(), linkTo(methodOn(PropertyController.class).all())
                                  .withRel("property"));
    }
}
