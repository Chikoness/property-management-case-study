package com.example.propertymanagementapi.Property;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface PropertyRepository extends JpaRepository<Property, Long>, JpaSpecificationExecutor<Property> {}
