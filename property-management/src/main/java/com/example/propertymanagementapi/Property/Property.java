package com.example.propertymanagementapi.Property;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Property {
    private @Id
    @GeneratedValue
    Long id;
    private String name;
    private String address;
    private int postcode;
    private String city;
    private String state;
    private boolean isPropertyApproved = false; // It will be false by default, since it needs to be approved

    Property() {}

    public Property(String name, String addressLine, int postcode, String city, String state) {
        this.name = name;
        this.address = addressLine;
        this.postcode = postcode;
        this.city = city;
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getPostcode() {
        return postcode;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPropertyApproved(boolean propertyApproved) {
        isPropertyApproved = propertyApproved;
    }

    public boolean isPropertyApproved() {
        return isPropertyApproved;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Property)) {
            return false;
        }

        Property property = (Property) obj;

        return Objects.equals(this.id, property.id)
                && Objects.equals(this.name, property.name)
                && Objects.equals(this.address, property.address)
                && Objects.equals(this.postcode, postcode)
                && Objects.equals(this.city, property.city)
                && Objects.equals(this.state, property.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.name, this.address, this.postcode, this.city, this.state);
    }

    @Override
    public String toString() {
        return "Property{" + "id=" + this.id + ", name='" + this.name
                + "', address='" + this.address + "', postcode=" + this.postcode + ", city='" + city
                + "', state='" + this.state + "'}";
    }
}