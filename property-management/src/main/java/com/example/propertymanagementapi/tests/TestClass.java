package com.example.propertymanagementapi.tests;

import com.example.propertymanagementapi.Property.Property;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.*;
public class TestClass extends AbstractTest {
    @Test
    public void tryAddProperty() throws Exception {
        Property property = new Property("Happy Apartment",
                "Jalan Gembira",
                55555,
                "Petaling Jaya",
                "Selangor");

        String inputJson = super.mapToJson(property);
        MvcResult res = mvc.perform(MockMvcRequestBuilders.post("/api/properties")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = res.getResponse().getStatus();
        assertEquals(201, status);
        String content = res.getResponse().getContentAsString();
        assertEquals(content, "Property is created successfully");
    }
}
