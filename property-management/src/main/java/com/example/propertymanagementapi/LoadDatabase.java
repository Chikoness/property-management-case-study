package com.example.propertymanagementapi;

import com.example.propertymanagementapi.Property.Property;
import com.example.propertymanagementapi.Property.PropertyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// Fake database to populate some preloaded properties
@Configuration
public class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(PropertyRepository repo) {
        return args -> {
            log.info("Preloading " + repo.save(new Property("Crest Residences",
                    "3 Jalan Cendana, Off Jalan Sultan Ismail",
                    50250,
                    "Kuala Lumpur",
                    "Kuala Lumpur")));
            log.info("Preloading " + repo.save(new Property("Zeta Suite",
                    "Taman Zeta Park, Jalan Taman Ibu Kota C-10-11 Zetapark Zen Suite",
                    53300,
                    "Kuala Lumpur",
                    "Kuala Lumpur")));
        };
    }
}
