# Speedhome's Property Management API Technical Test
## By Charlene A. @ https://github.com/Chikoness

## USERS
Firstly, there are 2 different roles for the users.

1. Admin (username: admin, password: admin123)
2.  User (username: user, password: user123)

Admin is assumed to be a property agent and/or the admin. Therefore, they are allowed to do EVERYTHING on the API. This includes :-

1. Creating a new property entry
2. View one of the property by finding its id
3. View all of the existing properties
4. Updating an existing property
5. Approving new properties created by User
6. Delete an existing property entry

Users are only allowed to perform option 1 - 3.

## HOW TO LOGIN :
1. After running the API, go to localhost:8085 on an API dev, such as Postman
2. POST desired username & password into the field
3. Get token from Body
4. Use in Header (Don't forget to add "Bearer " before your token key)
5. You will not be able to access anything without the token!!

## CREATE PROPERTY
New property can be created by using POST to JSON Body with the fields {name, address, postcode, city, state} to localhost:8085/api/properties

## VIEW A PROPERTY
Property can be viewed by using GET to localhost:8085/api/properties/(number)

## VIEW ALL PROPERTIES
Properties can be viewed by using GET to localhost:8085/api/properties/

## UPDATING AN EXISTING PROPERTY
An existing property can be updated by using PUT to JSON Body with the fields {name, address, postcode, city, state} to localhost:8085/api/properties/(number)
Failure to update to the right property number will result in an accidental new property created. (Which can be hidden since its propertyApproval value is "false" by default)

## APPROVING NEW PROPERTIES
An existing property can be approved by using PUT to JSON Body with the field {propertyApproval = true/false} to localhost:8085/api/properties/(number)/approval. Failure to update to the right property number will just result in a custom exception handling message to the screen.

## DELETE AN EXISTING PROPERTY ENTRY
An existing property can be deleted by using DELETE to localhost:8085/api/properties/(number).



## LIMITATIONS TO THIS API
1. Usernames and passwords were hard-coded and not created using database queries.

2. Only an in-memory database is used (H2). No physical storage exists.

3. The "approval" of property is only done using a boolean within the Property entity/object. This could probably be used in the front end to hide properties with propertyApproval of false.