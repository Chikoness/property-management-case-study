# Property Management Case Study

SCSJ4383 Software Construction

UTMSPACE KL 2021/2022 Sem 1 

Group 4

## Group members ##

Charlene Ng Andrew SX180355CSJS04

Nik Mohammad Faris B Nik Ismail SX180351CSJS04

NIK NUR ASHIKIN BINTI MEGAT SHAIDI SX170113CSJS04 

Auni Dalilah Binti Mohd Zain SX170101CSJS04

Kigendren Siva Kumar SX171065CSJS04

### Description ###

This is a case study to make a RESTful web service in Java Spring Boot for a property management app such as Speedhome, PropertyGuru, iProperty and so on.


### Functionalities ###

**1) CREATE PROPERTY**

- New property can be created by using POST to JSON Body with the fields {name, address, postcode, city, state} to localhost:8085/api/properties
VIEW A PROPERTY

- Property can be viewed by using GET to localhost:8085/api/properties/(number)

**2) VIEW ALL PROPERTIES**

- Properties can be viewed by using GET to localhost:8085/api/properties/

**3) UPDATING AN EXISTING PROPERTY**

- An existing property can be updated by using PUT to JSON Body with the fields {name, address, postcode, city, state} to localhost:8085/api/properties/(number) Failure to update to the right property number will result in an accidental new property created. (Which can be hidden since its propertyApproval value is "false" by default)

**4) APPROVING NEW PROPERTIES**

- An existing property can be approved by using PUT to JSON Body with the field {propertyApproval = true/false} to localhost:8085/api/properties/(number)/approval. Failure to update to the right property number will just result in a custom exception handling message to the screen.

**5) DELETE AN EXISTING PROPERTY ENTRY**

- An existing property can be deleted by using DELETE to localhost:8085/api/properties/(number).

**6) SEARCH A PROPERTY**

- An existing property can be searched by it's address. For example, to search all properties in the state of Selangor, simply type `localhost:8085/api/p?search=state:'Selangor'`